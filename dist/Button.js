"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _react = _interopRequireDefault(require("react"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
var Button = function Button(_ref) {
  var onClick = _ref.onClick,
    children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement("button", {
    onClick: onClick
  }, children);
};
var _default = exports["default"] = Button;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJfcmVhY3QiLCJfaW50ZXJvcFJlcXVpcmVEZWZhdWx0IiwicmVxdWlyZSIsIm9iaiIsIl9fZXNNb2R1bGUiLCJCdXR0b24iLCJfcmVmIiwib25DbGljayIsImNoaWxkcmVuIiwiY3JlYXRlRWxlbWVudCIsIl9kZWZhdWx0IiwiZXhwb3J0cyJdLCJzb3VyY2VzIjpbIi4uL3NyYy9CdXR0b24uanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5cbmNvbnN0IEJ1dHRvbiA9ICh7IG9uQ2xpY2ssIGNoaWxkcmVuIH0pID0+IHtcbiAgICByZXR1cm4gPGJ1dHRvbiBvbkNsaWNrPXtvbkNsaWNrfT57Y2hpbGRyZW59PC9idXR0b24+O1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQnV0dG9uOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBQUEsTUFBQSxHQUFBQyxzQkFBQSxDQUFBQyxPQUFBO0FBQTBCLFNBQUFELHVCQUFBRSxHQUFBLFdBQUFBLEdBQUEsSUFBQUEsR0FBQSxDQUFBQyxVQUFBLEdBQUFELEdBQUEsZ0JBQUFBLEdBQUE7QUFFMUIsSUFBTUUsTUFBTSxHQUFHLFNBQVRBLE1BQU1BLENBQUFDLElBQUEsRUFBOEI7RUFBQSxJQUF4QkMsT0FBTyxHQUFBRCxJQUFBLENBQVBDLE9BQU87SUFBRUMsUUFBUSxHQUFBRixJQUFBLENBQVJFLFFBQVE7RUFDL0Isb0JBQU9SLE1BQUEsWUFBQVMsYUFBQTtJQUFRRixPQUFPLEVBQUVBO0VBQVEsR0FBRUMsUUFBaUIsQ0FBQztBQUN4RCxDQUFDO0FBQUMsSUFBQUUsUUFBQSxHQUFBQyxPQUFBLGNBRWFOLE1BQU0iLCJpZ25vcmVMaXN0IjpbXX0=